//
//  sccooler.cxx
//  
//
//  Created by Giacomo Gallina on 09/03/17.
//
//
//  This MIDAS file manage the COOLER for nEXO experiment
//
//
//  Usage: From Home/online/sccooler directory run the executabale ./sccooler
//  To compile it, use "make" from the same directory


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
//#include "mscb.h"
#include "mfe.h"
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <sys/time.h> // gettimeofday()
#include "TimeDerivative.hh"

//FOR RS232

#include "rs232.h"

/* make frontend functions callable from the C framework */
//#ifdef __cplusplus
//extern "C" {
//#endif

/* do not read Common from ODB, overwrite it, TRUE is standard behaviour */
BOOL equipment_common_overwrite = TRUE;

/*-- Globals -------------------------------------------------------*/


//GLOBAL VARIABLES

int cport_nr = RS232_GetPortnr("ttyUSB0");        /* /dev/ttyUSB0 on Linux machine*/
//int bdrate=50;
int bdrate=38400;

char mode[]={'8','N','1',0};
char str[2][512];
unsigned char buf[4096];

//Some strange erros that sometimes happens
unsigned char asterish='*';

unsigned char small_dot='.';

char zero_string[]="-0.000.";
int old_value=0;

int busy;//To avoid to write in the same time

/* The frontend name (client name) as seen by other MIDAS clients   */
char const *frontend_name = "scCooler";

/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;


//HNDLE hDB, hDD, hSet, hControl;
HNDLE hDD, hSet, hControl;

// derivative class
TimeDerivative* gD = new TimeDerivative(1,51);

// Timestamp function
double GetTime()
{
   struct timeval tv;
   gettimeofday(&tv, NULL);
   return tv.tv_sec*1.0 + tv.tv_usec/1000000.0;
}



#define COOLER_SETTINGS_STR(_name) char const *_name[] = {\
  "Prop_PID_Cooling = FLOAT : 20.0",\
  "Derivative_PID_Cooling = FLOAT : 2.0",\
  "Integrative_PID_Cooling = FLOAT : 0.1",\
  "Final_temperature = FLOAT : 20",\
  "HOLD_MODE = INT : 0",\
  "Actual_temp = FLOAT : 20",\
  "",	\
  NULL }

typedef struct {
  float   Prop_PID_Cooling;
  float   Derivative_PID_Cooling;
  float   Integrative_PID_Cooling;
  float Final_temperature;
  float Actual_temp;
  int  HOLD_MODE;
} COOLER_SETTINGS;


COOLER_SETTINGS COOLER_settings;

int lmscb_fd;
HNDLE hmyFlag;

/*-- Function declarations -----------------------------------------*/

//MIDAS FUNCTION
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_temperature_COOLER(char *pevent, INT off);
INT read_Cooler_io_event(char *pevent, INT off);
void param_callback(INT hDB, INT hKey, void *info);
void register_cnaf_callback(int debug);
void seq_callback(INT hDB, INT hseq, void *info);
void seq_HOLD_FINAL_TEMPERATURE(INT hDB, INT hseq, void *info);


//#ifdef __cplusplus
//    extern "C" {
//#endif

EQUIPMENT equipment[] = {

  { "SCCOOLER",                 /* equipment name */
    {
      443,
      0,     /* event ID, trigger mask */
      "SYSTEM",              /* event buffer */
      EQ_PERIODIC ,      /* equipment type */
      0,     /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS,             /* read always */
      5,                    /* read every 5 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",
    },
    read_Cooler_io_event,       /* readout routine */
  },
  {
    "COOLER_Temperature",             /* equipment name */
    {
      494,
      0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
        RO_ODB,                 /* and update ODB */
      1,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_temperature_COOLER,       /* readout routine */
  },
  {""}
};

//----------------------------------------------------------------




//#ifdef __cplusplus
//    }
//#endif

/********************************************************************\
  Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
occations:

frontend_init:  When the frontend program is started. This routine
should initialize the hardware.

frontend_exit:  When the frontend program is shut down. Can be used
to releas any locked resources like memory, commu-
nications ports etc.

begin_of_run:   When a new run is started. Clear scalers, open
rungates, etc.

end_of_run:     Called on a request to stop a run. Can send
end-of-run event and close run gates.

pause_run:      When a run is paused. Should disable trigger events.

resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/


/*-- Sequencer callback info  --------------------------------------*/


//PID LOOP
void seq_PID_Cooling(INT hDB, INT hseq, void *info)
{


  strcpy(str[0], "TEMP:CPID ");

  char settaggio[64];

  printf("Changing Cooling PID loop terms...\n\n");
  printf("New values...:\n");
  //printf("COOLER_settings.Prop_PID_Cooling=%f\n",COOLER_settings.Prop_PID_Cooling);
  //printf("COOLER_settings.Prop_PID_Cooling=%f\n",COOLER_settings.Derivative_PID_Cooling);
  //printf("COOLER_settings.Prop_PID_Cooling=%f\n",COOLER_settings.Integrative_PID_Cooling);

  sprintf(settaggio,"%f,",COOLER_settings.Prop_PID_Cooling);

  strcat(str[0],settaggio);

  sprintf(settaggio,"%f,",COOLER_settings.Derivative_PID_Cooling);

  strcat(str[0],settaggio);

  sprintf(settaggio,"%f\n",COOLER_settings.Integrative_PID_Cooling);

  strcat(str[0],settaggio);

  puts(str[0]);

  //SEND COMMAND
  RS232_cputs(cport_nr, str[0]);



}



//STOP CONTROLLER
void seq_Stop_Cooling_2(void)
{


  printf("Stopping Cooler...\n\n");


  strcpy(str[0], "TEMP:STOP\n");

  while(busy==1){
    printf("Wasting time...\n");

  }

  busy=1;
  //SEND COMMAND
  RS232_cputs(cport_nr, str[0]);

  busy=0;

  ss_sleep(1000);

  printf("Cooler stopped\n\n");


}


//ENEBLE HOLD MODE WITH DESIDERED FINAL TEMPERATURE
void seq_HOLD_FINAL_TEMPERATURE(INT hDB, INT hseq, void *info)
{


  std::cout<<"Going"<<std::endl;

  float temp_to_reach;
  int size,status;
  int run;



  size = sizeof(run);
  status = db_get_value(hDB, 0,"/Equipment/scCOOLER/Settings/HOLD_MODE", &run, &size, TID_INT, FALSE);



  if(run>1 || run<0){

    printf("Wrong value, only permitted values are 0 and 1\n");


  }else{


    if(run==0){
      seq_Stop_Cooling_2();
      return;

    }


    size = sizeof(temp_to_reach);
    status = db_get_value(hDB, 0,"/Equipment/scCOOLER/Settings/Final_temperature", &temp_to_reach, &size, TID_FLOAT, FALSE);


    strcpy(str[0], "TEMP:HOLD ");

    char settaggio[64];

    printf("HOLDING MODE ENABLED, reaching desidered temperature...\n\n");

    sprintf(settaggio,"%f\n",temp_to_reach);

    strcat(str[0],settaggio);

    puts(str[0]);

    while(busy==1){

      printf("Wasting time...\n");
    }

    busy=1;

    //Wait before to give the command
    ss_sleep(1000);

    //SEND COMMAND
    RS232_cputs(cport_nr, str[0]);

    ss_sleep(1000);

    busy=0;
  }

}




/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{



  printf("\n\n\
      **********************************************************\n\
      *                                                        *\n\
      *                  MIDAS  COOLER INTERFACE               *\n\
      *                                                        *\n\
      *                Management of COOLING stage             *\n\
      *                    for nEXO experiment                 *\n\
      *                                                        *\n\
      *                                                        *\n\
      **********************************************************\n\n");

  //MIDAS
  char  mscbstr[64];
  int status = 1, size;
  char set_str[80];

  int result;
  const char *err;


  COOLER_SETTINGS_STR(COOLER_settings_str);

  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */

  cm_get_experiment_database(&hDB, NULL);

  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/%s/Settings", "scCOOLER");


  /* create PARAM settings record */
  status = db_create_record(hDB, 0, set_str, strcomb(COOLER_settings_str));
  if (status != DB_SUCCESS)  return FE_ERR_ODB;
  status = db_find_key(hDB, 0, set_str, &hSet);

  if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);


  printf("Opening COOLER device using SERIAL RS 232 port ...\n");
  printf("Brate=%d\n",bdrate);
  printf("Connection Port=%d\n",cport_nr);





  if(RS232_OpenComport(cport_nr, bdrate, mode))
  {
    printf("Cannot open COOLER, check connection or cooler!\n");

    return(0);

  }else{

    printf("COOLER opened!\n");

  }




  //SET DEFALUT VALUES FOR THE COOLING SYSTEM. ONLY ON SCREEN NOT IN COOLER




  //SET DEFALUT VALUES FOR THE COOLING SYSTEM. IN COOLER

  printf("Loading default values in COOLER.....\n\n");


  printf("Loading Proportional COLLING term.....\n");

  float flag=20.0;
  db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/Prop_PID_Cooling", &flag, sizeof(flag), 1, TID_FLOAT);


  printf("Loading Derivative COLLING term.....\n");

  flag=2.0;
  db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/Derivative_PID_Cooling", &flag, sizeof(flag), 1, TID_FLOAT);


  printf("Loading Integrative COLLING term.....\n");

  flag=0.1;
  db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/Integrative_PID_Cooling", &flag, sizeof(flag), 1, TID_FLOAT);

  //Room temperature
  flag=20.0;
  db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/Final_temperature", &flag, sizeof(flag), 1, TID_FLOAT);


  int flag_3=0;
  db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/HOLD_MODE", &flag_3, sizeof(flag_3), 1, TID_INT);

  printf("Stop the cooler.....\n");


  printf("Loading default values DONE.....!\n\n");


  //-------------Hot link initialization --------------//

  printf("HOT links creation....\n");

  //Proportional term
  status = db_find_key(hDB, 0, "/Equipment/scCOOLER/Settings/Prop_PID_Cooling", &hmyFlag);
  if (status == DB_SUCCESS) {
    /* Enable hot-link on settings/ of the equipment */
    if (hmyFlag) {
      size = sizeof(BOOL);
      if ((status = db_open_record(hDB, hmyFlag, &(COOLER_settings.Prop_PID_Cooling)
              , size, MODE_READ
              , seq_PID_Cooling, NULL)) != DB_SUCCESS)
        return status;
    }
  }else{
    cm_msg(MERROR, "feSCCOOLER", "cannot access output");
  }

  //Derivative term
  status = db_find_key(hDB, 0, "/Equipment/scCOOLER/Settings/Derivative_PID_Cooling", &hmyFlag);
  if (status == DB_SUCCESS) {
    /* Enable hot-link on settings/ of the equipment */
    if (hmyFlag) {
      size = sizeof(BOOL);
      if ((status = db_open_record(hDB, hmyFlag, &(COOLER_settings.Derivative_PID_Cooling)
              , size, MODE_READ
              , seq_PID_Cooling, NULL)) != DB_SUCCESS)
        return status;
    }
  }else{
    cm_msg(MERROR, "feSCCOOLER", "cannot access output");
  }

  //Integrative term
  status = db_find_key(hDB, 0, "/Equipment/scCOOLER/Settings/Integrative_PID_Cooling", &hmyFlag);
  if (status == DB_SUCCESS) {
    /* Enable hot-link on settings/ of the equipment */
    if (hmyFlag) {
      size = sizeof(BOOL);
      if ((status = db_open_record(hDB, hmyFlag, &(COOLER_settings.Integrative_PID_Cooling)
              , size, MODE_READ
              , seq_PID_Cooling, NULL)) != DB_SUCCESS)
        return status;
    }
  }else{
    cm_msg(MERROR, "feSCCOOLER", "cannot access output");
  }


  //Hold final temperature
  status = db_find_key(hDB, 0, "/Equipment/scCOOLER/Settings/HOLD_MODE", &hmyFlag);
  if (status == DB_SUCCESS) {
    /* Enable hot-link on settings/ of the equipment */
    if (hmyFlag) {
      size = sizeof(BOOL);
      if ((status = db_open_record(hDB, hmyFlag, &(COOLER_settings.HOLD_MODE)
              , size, MODE_READ
              , seq_HOLD_FINAL_TEMPERATURE, NULL)) != DB_SUCCESS)
        return status;
    }
  }else{
    cm_msg(MERROR, "feSCCOOLER", "cannot access output");
  }




  printf("HOT links creation....DONE\n\n");

  return FE_SUCCESS;

}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{

  printf("Closing...\n");

  //Closing serial PORT
  RS232_CloseComport(cport_nr);

  return SUCCESS;

}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{


  HNDLE hDB;
  int size,status;

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events


/*-- Trigger event routines ----------------------------------------*/


//extern "C" INT poll_event(INT source, INT count, BOOL test)
INT poll_event(INT source, INT count, BOOL test)
  /* Polling routine for events. Returns TRUE if event
     is available. If test equals TRUE, don't return. The test
     flag is used to time the polling */
{
  int i;
  DWORD lam;

  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
        return lam;
  }
  return 0;
}



/*-- Interrupt configuration ---------------------------------------*/
//extern "C" INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_ATTACH:
      break;
    case CMD_INTERRUPT_DETACH:
      break;
  }
  return SUCCESS;
}

/*-- event --------------------------------------------------*/


INT read_Cooler_io_event(char *pevent, INT off)
{
  int status, size;
  float *pfdata;
  char mscbstr[64];
  int freq,output,dutycycle;
  int out_global=0;
  /* init bank structure */
  bk_init(pevent);

  /* create Phidget bank */
  bk_create(pevent, "COOL", TID_FLOAT, (void **) &pfdata);


  bk_close(pevent, pfdata);


  return 0;

  printf("event size %d\n", bk_size(pevent));

  return bk_size(pevent);
}




//ROUTINE TO MANAGE THE READING OF TEMPERATURE


INT read_temperature_COOLER(char *pevent, INT off) {


  busy=0;

  std::cout<<"begin of reading cycle "<<std::endl;        

  int i,n=0;
  double *pdata;
  float temp;
  int addr;
  int flagset=0;
  int odbvalue;
  double time;
  double *pderv;
  double deriv;


  /*
     int size = sizeof(odbvalue);
     db_get_value(hDB, 0,"/Equipment/scPico/Settings/TAKE_IV_CURVE", &odbvalue, &size, TID_INT, FALSE);        


     if(odbvalue==5)
     {
     ss_sleep(4000);
     flagset=1;

     db_set_value(hDB,0,"/Equipment/scPico/Settings/TAKE_IV_CURVE", &flagset, sizeof(flagset), 1, TID_INT);

     }
     else if(odbvalue==4)
     {
     ss_sleep(4000);
     flagset=3;

     db_set_value(hDB,0,"/Equipment/scPico/Settings/TAKE_IV_CURVE", &flagset, sizeof(flagset), 1, TID_INT);

     }

*/

  while(busy==1){

    //Do nothing
    printf("Wasting time...\n");

  }

  //Set as not free
  busy=1;


  bk_init32(pevent);

  // create data bank for temperature
  bk_create(pevent, "CTEM", TID_DOUBLE, (void **)&pdata);

  // prepare command to send
  strcpy(str[0], "TEMPerature:CTEMperature?\n");


  //SEND COMMAND
  RS232_cputs(cport_nr, str[0]);


  while(1)
  {

    //Wait before to receive the answer
    ss_sleep(1000);
    //RICEVO RISPOSTA
    n = RS232_PollComport(cport_nr, buf, 4095);
    time = GetTime();
    
    std::cout<<"xx"<<std::endl;

    if(n > 0)
    {
      buf[n] = 0;   /* always put a "null" at the end of a string! */

      for(i=0; i < n; i++)
      {
        if(buf[i] < 32)  /* replace unreadable control-codes by dots */
        {
          buf[i] = '.';
        }
      }

      //Avoid errors
      if(buf[0]!=asterish && buf[0]!=small_dot){

        //Avoid other strange error that appears in a particular case
        if(strcmp((char*)buf,zero_string)==0){

          printf("An error happens....Ship line...\n");
          break;


        }

        //NO error happens

        temp=atof((char*)buf);

        printf("Received %i bytes, Cooler Temperature: %s at %1.12e\n", n, (char *)buf, time);
        //printf("pluto=%f\n",temp);


        if(temp!=0){
          break;
        }


      }else{
        //An error happens
        printf("An error happens....Ship line...\n");
        break;
      }
    }


    std::cout<<"Nothing Received"<<std::endl;

    ss_sleep(1000);

  }

  *pdata++ =  temp;
  bk_close(pevent,pdata);

  // create data bank for temperature derivative
  bk_create(pevent, "TDRV", TID_DOUBLE, (void **)&pderv);

  double meas = static_cast<double>(temp);
  gD->BackWardDiff_Harbitrary(&time,&meas);
  deriv = gD->GetDeriv1();
  printf("\t Derivative: %1.6e\n",deriv);
  
  *pderv++ =  deriv;
  bk_close(pevent,pderv);

  //Now it is free

  busy=0;


  return bk_size(pevent);
}

//}



