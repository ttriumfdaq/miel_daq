# MIEL DAQ

Data AcQuisition with CAEN DT5xxx and Phidgets

## Prerequisites

- CAENUSBdrvB-1.5.4
- CAENComm-1.5.0
- CAENVMELib-3.3.0
- libphidget22

## Installation

```
mkdir build
cd build
cmake ..
cmake --build .
```

## Quick Start

```
cd bin
./start_daq.sh
```

## Data Analysis

[MERCI](https://bitbucket.org/team-waveformers/merci/src/devel/)
