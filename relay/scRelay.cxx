//
//  scRelay.cxx
//  
//
//  Created by Giacomo Gallina on 3/04/2020 .
//
//  OUTPUT
//
//  In "Hystory" in the ODB I display the Relay State (LOW or HIGH)
//
//  NO SETTINGS ONLY VARIABLES
//
//  Usage: From Home/online/relay directory run the executabale ./scRelay
//  To compile it, use "make" from the same directory.
//
// Phydget Configuration:
//
// Channel 4: Relay for Pressure guage



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "mfe.h"
// #include "mscb.h"
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <phidget22.h>


/*-- Globals -------------------------------------------------------*/
    
    
    
//Declare a relay object
PhidgetDigitalOutputHandle ifKit = 0;

PhidgetReturnCode prc; //Used to catch error codes from 1 Phidget function call

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "scRelay";
/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;
    
/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;
    
/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;
    
/* maximum event size produced by this frontend */
INT max_event_size = 10000;
    
/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;
    
/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;
    
//char const mydevname[] = {"GPIB410"};
extern HNDLE hDB, hDD, hSet, hControl;

    
    
int lmscb_fd;
HNDLE hmyFlag;



#define SCRELAY_SETTINGS_STR(_name) char const *_name[] = {\
"STATE = INT : 0",\
"",    \
NULL }
    
    typedef struct {
     float   STATE;
    } RELAY_SETTINGS;
    
    
    RELAY_SETTINGS RELAY_settings;


    
/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
    
INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);
void param_callback(INT hDB, INT hKey, void *info);
void register_cnaf_callback(int debug);
void seq_callback(INT hDB, INT hseq, void *info);
    
//Read relay_state periodically
INT read_state(char *pevent, INT off);

    
    
/*-- Equipment list ------------------------------------------------*/
        
      
        
EQUIPMENT equipment[] = {
  {
    "scRelay",             /* equipment name */
    {
      396, 0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_state,       /* readout routine */
  },
  {""}
};
        


//SET STATE CHANNEL 1

void set_state(INT hDB, INT hseq, void *info){


    int state;
    int status, size;
    size = sizeof(state);
    status = db_get_value(hDB, 0,"/Equipment/scRelay/Settings/STATE", &state, &size, TID_INT, FALSE);

    PhidgetDigitalOutput_setState(ifKit, state);
    
    
    ss_sleep(1000);


}


        
//----------------------------------------------------------------

//Generic Handler Management
void DisplayError(PhidgetReturnCode returnCode, const char* message) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  const char* error;
            
  fprintf(stderr, "Runtime Error -> %s\n\t", message);
            
  prc = Phidget_getErrorDescription(returnCode, &error);
  if (prc != EPHIDGET_OK) {
    DisplayError(prc, "Getting ErrorDescription");
    return;
  }
            
  fprintf(stderr, "Desc: %s\n", error);
            
  if (returnCode == EPHIDGET_WRONGDEVICE) {
    fprintf(stderr, "\tThis error commonly occurs when the Phidget function you are calling does not match the class of the channel that called it.\n"
	    "\tFor example, you would get this error if you called a PhidgetVoltageInput_* function with a PhidgetDigitalOutput channel.");
  }
  else if (returnCode == EPHIDGET_NOTATTACHED) {
    fprintf(stderr, "\tThis error occurs when you call Phidget functions before a Phidget channel has been opened and attached.\n"
	    "\tTo prevent this error, ensure you are calling the function after the Phidget has been opened and the program has verified it is attached.\n");
  }
  else if (returnCode == EPHIDGET_NOTCONFIGURED) {
    fprintf(stderr, "\tThis error code commonly occurs when you call an Enable-type function before all Must-Set Parameters have been set for the channel.\n"
	    "\tCheck the API page for your device to see which parameters are labled \"Must be Set\" on the right-hand side of the list.");
  }
}
        
//Generic Handler Management
void ExitWithErrors(PhidgetHandle *chptr) {
  Phidget_delete(chptr);
  fprintf(stderr, "\nExiting with error(s)...\n");
  printf("Press ENTER to end program.\n");
  getchar();
  exit(1);
}
 
//Generic Handler Management
void CheckError(const PhidgetReturnCode returnCode, const char* message, PhidgetHandle *chptr) {
            
  if (returnCode != EPHIDGET_OK) {
    DisplayError(returnCode, message);
    ExitWithErrors(chptr);
  }
}

//Generic Handler Management
static void CCONV onAttachHandler(PhidgetHandle ph, void *ctx) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  Phidget_DeviceClass deviceClass;
  const char* channelClassName;
  int32_t serialNumber;
  int32_t hubPort;
  int32_t channel;
            
  //If you are unsure how to use more than one Phidget channel with this event, we recommend going to
  //www.phidgets.com/docs/Using_Multiple_Phidgets for information
            
  printf("\nAttach Event: ");
  /*
   * Get device information and display it.
   */
  prc = Phidget_getDeviceSerialNumber(ph, &serialNumber);
  CheckError(prc, "Getting DeviceSerialNumber", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannel(ph, &channel);
  CheckError(prc, "Getting Channel", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannelClassName(ph, &channelClassName);
  CheckError(prc, "Getting Channel Class Name", (PhidgetHandle *)&ph);
            
  prc = Phidget_getDeviceClass(ph, &deviceClass);
  CheckError(prc, "Getting Device Class", (PhidgetHandle *)&ph);
            
  if (deviceClass == PHIDCLASS_VINT) {
    prc = Phidget_getHubPort(ph, &hubPort);
    CheckError(prc, "Getting HubPort", (PhidgetHandle *)&ph);
                
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Hub Port: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, hubPort, channel);
  } else { //Not VINT
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, channel);
  }
            
            
}

//Generic Handler Management
static void CCONV onDetachHandler(PhidgetHandle ph, void *ctx) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  Phidget_DeviceClass deviceClass;
  const char* channelClassName;
  int32_t serialNumber;
  int32_t hubPort;
  int32_t channel;
            
  //If you are unsure how to use more than one Phidget channel with this event, we recommend going to
  //www.phidgets.com/docs/Using_Multiple_Phidgets for information
            
  printf("\nDetach Event: ");
  /*
   * Get device information and display it.
   */
  prc = Phidget_getDeviceSerialNumber(ph, &serialNumber);
  CheckError(prc, "Getting DeviceSerialNumber", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannel(ph, &channel);
  CheckError(prc, "Getting Channel", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannelClassName(ph, &channelClassName);
  CheckError(prc, "Getting Channel Class Name", (PhidgetHandle *)&ph);
            
  prc = Phidget_getDeviceClass(ph, &deviceClass);
  CheckError(prc, "Getting Device Class", (PhidgetHandle *)&ph);
            
  if (deviceClass == PHIDCLASS_VINT) {
    prc = Phidget_getHubPort(ph, &hubPort);
    CheckError(prc, "Getting HubPort", (PhidgetHandle *)&ph);
                
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Hub Port: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, hubPort, channel);
  } else { //Not VINT
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, channel);
  }
}

//Generic Handler Management
static void CCONV onErrorHandler(PhidgetHandle ph, void *ctx, Phidget_ErrorEventCode errorCode, const char *errorString) {
            
  fprintf(stderr, "[Phidget Error Event] -> %s (%d)\n", errorString, errorCode);
}

//Generic Handler Management
void CheckOpenError(PhidgetReturnCode e, PhidgetHandle *chptr) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  Phidget_ChannelClass channelClass;
  int isRemote;
            
  if (e == EPHIDGET_OK)
    return;
            
  DisplayError(e, "Opening Phidget Channel");
  if (e == EPHIDGET_TIMEOUT) {
    fprintf(stderr, "\nThis error commonly occurs if your device is not connected as specified, "
	    "or if another program is using the device, such as the Phidget Control Panel.\n\n"
	    "If your Phidget has a plug or terminal block for external power, ensure it is plugged in and powered.\n");
                
    prc = Phidget_getChannelClass(*chptr, &channelClass);
    CheckError(prc, "Getting ChannelClass", chptr);
                
    if (channelClass != PHIDCHCLASS_VOLTAGEINPUT
	&& channelClass != PHIDCHCLASS_VOLTAGERATIOINPUT
	&& channelClass != PHIDCHCLASS_DIGITALINPUT
	&& channelClass != PHIDCHCLASS_DIGITALOUTPUT) {
      fprintf(stderr, "\nIf you are trying to connect to an analog sensor, you will need to use the "
	      "corresponding VoltageInput or VoltageRatioInput API with the appropriate SensorType.\n");
    }
                
    prc = Phidget_getIsRemote(*chptr, &isRemote);
    CheckError(prc, "Getting IsRemote", chptr);
                
    if (isRemote)
      fprintf(stderr, "\nEnsure the Phidget Network Server is enabled on the machine the Phidget is plugged into.\n");
  }
            
  ExitWithErrors(chptr);
}
        
      
//----------------------------------------------------------------
        
        
        
/*-- Sequencer callback info  --------------------------------------*/
    
    
void seq_callback(INT hDB, INT hseq, void *info)
{
  printf("ODB Settings are only for display properties\n");

}
    
    

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{

  int status = 1;
  int size;
  char set_str[80];
    
        
  //General Board Information
        
  int32_t serialNumber=561108;//S/N  Phydget Hub
  int32_t hubPort_phydget=3; //Hub Port in which the relay is attached
  int32_t channel=0;
        
   
    SCRELAY_SETTINGS_STR(SCRELAY_settings_str);
    
    /* hardware initialization */
    /* print message and return FE_ERR_HW if frontend should not be started */
    
    cm_get_experiment_database(&hDB, NULL);
    
    /* Map /equipment/Trigger/settings for the sequencer */
    sprintf(set_str, "/Equipment/%s/Settings", "scRelay");
    
    
    /* create PARAM settings record */
    status = db_create_record(hDB, 0, set_str, strcomb(SCRELAY_settings_str));
    if (status != DB_SUCCESS)  return FE_ERR_ODB;
    //status = db_find_key(hDB, 0, set_str, &hSet);

    if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
    
    
    printf("HOT links creation ....\n");

    status = db_find_key(hDB, 0, "/Equipment/scRelay/Settings/STATE", &hmyFlag);
    if (status == DB_SUCCESS) {
        /* Enable hot-link on settings/ of the equipment */
        if (hmyFlag) {
            size = sizeof(BOOL);
            if ((status = db_open_record(hDB, hmyFlag, &(RELAY_settings.STATE)
                                         , size, MODE_READ
                                         , set_state, NULL)) != DB_SUCCESS)
                return status;
        }
    }else{
        cm_msg(MERROR, "fepico", "cannot access set_STATE");
    }
    
    
    printf("HOT links creation .... DONE ! \n");
    
    
  printf("\n\n\
               **********************************************************\n\
               *                                                        *\n\
               *                   MIDAS PHYDGET INTERFACE              *\n\
               *                                                        *\n\
               *                 Management of Relay board              *\n\
               *                   for nEXO experiment                  *\n\
               *                                                        *\n\
               *                                                        *\n\
               **********************************************************\n\n");
        
        
  printf("NOTE: The Hub parameters are coded IN !! ....\n\n");
  printf("If you change HUB or channel code must be changed ....\n\n");
        
        
  printf("Starting initialization....\n\n");

  printf("PHIDGET BOARD initialization....\n");
        
  /*Phidget initialization*/
        
  //create the InterfaceKit object 1
  PhidgetDigitalOutput_create(&ifKit);
                
  //Open the device
        
  prc = Phidget_setDeviceSerialNumber((PhidgetHandle)ifKit, serialNumber);
  CheckError(prc, "Setting DeviceSerialNumber", (PhidgetHandle *)&ifKit);
        
  prc = Phidget_setHubPort((PhidgetHandle)ifKit, hubPort_phydget);
  CheckError(prc, "Setting HubPort", (PhidgetHandle *)&ifKit);
        
  prc = Phidget_setChannel((PhidgetHandle)ifKit, channel);
  CheckError(prc, "Setting Channel", (PhidgetHandle *)&ifKit);
        
	
  /*                                                                                                                                                                                       
   * Add event handlers before calling open so that no events are missed.                                                                                                                   
   */

    
  //Wait for Phidget Relay Attachment
  printf("\n--------------------------------------\n");
  printf("\nSetting OnAttachHandler Board 1...\n");
  prc = Phidget_setOnAttachHandler((PhidgetHandle)ifKit, onAttachHandler, NULL);
  CheckError(prc, "Setting OnAttachHandler", (PhidgetHandle *)&ifKit);
        
  printf("Setting OnDetachHandler Board 1...\n");
  prc = Phidget_setOnDetachHandler((PhidgetHandle)ifKit, onDetachHandler, NULL);
  CheckError(prc, "Setting OnDetachHandler", (PhidgetHandle *)&ifKit);
        
  printf("Setting OnErrorHandler Board 1...\n");
  prc = Phidget_setOnErrorHandler((PhidgetHandle)ifKit, onErrorHandler, NULL);
  CheckError(prc, "Setting OnErrorHandler", (PhidgetHandle *)&ifKit);
        

  /*
   * Open the channel with a timeout
   */
    
    
  printf("Opening and Waiting for Attachment board 1...\n");
  prc = Phidget_openWaitForAttachment((PhidgetHandle)ifKit, 5000);
  CheckOpenError(prc, (PhidgetHandle *)&ifKit);


  printf("PHIDGET BOARD initialization....DONE\n\n");

    
    
    //Set the State of the relay to 0 (predefined value)

    int state=0;
    size = sizeof(state);
    db_set_value(hDB,0,"/Equipment/scRelay/Settings/STATE", &state, sizeof(state), 1, TID_INT);
    
    
    
        
   
  return FE_SUCCESS;
        
}
    
/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  printf("Closing...\n");

  PhidgetDigitalOutput_delete(&ifKit);
        
        
  return SUCCESS;
        
}
    
/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  /*
    if(status != DB_SUCCESS)
    {
    cm_msg(MERROR,"BOR","cannot get value");
    return FE_ERR_ODB;
    }
  */
        
  return SUCCESS;
}
    
/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
        
  return SUCCESS;
}
    
/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}
    
/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}
    
/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}
    
/*------------------------------------------------------------------*/
// Readout routines for different events
/*-- Trigger event routines ----------------------------------------*/
    
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int i;
  DWORD lam;
        
  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
	return lam;
  }
  return 0;
}
    
/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}
    
/*-- event --------------------------------------------------*/
    

//READ STATE CHANNEL 1 and save in history

INT read_state(char *pevent, INT off) {


  bk_init32(pevent);

  double *pdata;
  bk_create(pevent, "STAT", TID_DOUBLE, (void **)&pdata);

  int State;


  PhidgetDigitalOutput_getState((PhidgetDigitalOutputHandle)ifKit, &State);

    std::cout << "The current state is: "<<State<<" that is ";

    if(State==0) std::cout<<" low"<<std::endl;
    if(State==1) std::cout<<" high"<<std::endl;


    *pdata++ =  State;

    bk_close(pevent,pdata);


    return bk_size(pevent);


}












