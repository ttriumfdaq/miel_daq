#include "TimeDerivative.hh"


TimeDerivative::TimeDerivative(size_t nmax): fn(nmax)
{
  y1 = std::vector<double>(fn, 0.);
  y2 = std::vector<double>(fn, 0.);
  y3 = std::vector<double>(fn, 0.);
  t1 = std::vector<double>(fn, 0.);
  t2 = std::vector<double>(fn, 0.);
  t3 = std::vector<double>(fn, 0.);

  dydt = std::vector<double>(fn, 0.);
}

TimeDerivative::TimeDerivative(size_t nmax, unsigned int nsteps): fn(nmax), fs(nsteps)
{
  ybuff = std::vector<std::queue<double>>(fn);
  tbuff = std::vector<std::queue<double>>(fn);

  dydt = std::vector<double>(fn, 0.);
}

TimeDerivative::~TimeDerivative()
{
  ybuff.clear();
  tbuff.clear();
    
  y1.clear();
  y2.clear();
  y3.clear();
  t1.clear();
  t2.clear();
  t3.clear();

  dydt.clear();
}
  
// Compute 1st derivative using the backward finite difference method
// this should give an accuracy of O(t^3) 
// from https://en.wikipedia.org/wiki/Finite_difference_coefficient#Backward_finite_difference
void TimeDerivative::BackWardDiffO3(const std::vector<double>* xv, const std::vector<double>* yv)
{
  double x,y,h,dydx;
  for(size_t i=0; i<yv->size(); ++i)
    {
      x = xv->at(i);
      y = yv->at(i);
      if( std::isnan(y) ) y = 0.;
      h = x-t1[i];
      // std::cout<<"h: "<<h<<std::endl;
      dydx = (11.*y - 18.*y1[i] + 9.*y2[i] - 2.*y3[i])/(6.*h);
      if( std::isnan(dydx) ) dydt[i] = 0.;
      else dydt[i] = dydx;
      // fill buffer
      t3[i] = t2[i];
      t2[i] = t1[i];
      t1[i] = x;
      y3[i] = y2[i];
      y2[i] = y1[i];
      y1[i] = y;
    }
}
  
// Compute 1st derivative using the backward finite difference method
void TimeDerivative::BackWardDiff(const std::vector<double>* xv, const std::vector<double>* yv)
{
  double x,y,h,dydx;
  for(size_t i=0; i<yv->size(); ++i)
    {
      x = xv->at(i);
      y = yv->at(i);
      if( std::isnan(y) ) y = 0.;
      h = x-t1[i];
      //std::cout<<"h: "<<h<<std::endl;
      dydx = (y - y1[i])/h;
      if( std::isnan(dydx) ) dydt[i] = 0.;
      else dydt[i] = dydx;
      t1[i] = x;
      y1[i] = y;
    }
}

// Compute 1st derivative using the backward finite difference method
// the step size is arbitrary
void TimeDerivative::BackWardDiff_Harbitrary(const std::vector<double>* xv, const std::vector<double>* yv)
{
  double x,y,h,ytemp,dydx;
  for(size_t i=0; i<yv->size(); ++i)
    {
      // grab the variables
      x = xv->at(i); // this is Time
      y = yv->at(i); // this is measurement, e.g., Temperature
      // verify that the measurement makes sense
      if( std::isnan(y) ) y = 0.;

      // fill the buffer by sensor
      tbuff[i].push(x);
      ybuff[i].push(y);

      // if the buffer has not reached the desired size, continue
      if( ybuff[i].size() <= fs ) continue;
      else
	{
	  // if the buffer has reached the desired size
	  // get the oldest measurement
	  ytemp = ybuff[i].front();
	  ybuff[i].pop();

	  // and get the oldest time to compute the desired step
	  // typically h = fs x readout period
	  h = x - tbuff[i].front();
	  tbuff[i].pop();
	  std::cout<<"h: "<<h<<std::endl;
	}

      // compute the backward derivative
      dydx = (y - ytemp)/h;
      if( std::isnan(dydx) ) dydt[i] = 0.;
      else dydt[i] = dydx;
    }
}


// Compute 1st derivative using the backward finite difference method
// the step size is arbitrary
void TimeDerivative::BackWardDiff_Harbitrary(const double* x, const double* y)
{
  double h,ytemp,dydx;
  // verify that the measurement makes sense
  if( std::isnan(*y) ) return;

  // fill the buffer by sensor
  tbuff[0].push(*x);
  ybuff[0].push(*y);

  // if the buffer has not reached the desired size, continue
  if( ybuff[0].size() <= fs ) return;
  else
    {
      // if the buffer has reached the desired size
      // get the oldest measurement
      ytemp = ybuff[0].front();
      ybuff[0].pop();

      // and get the oldest time to compute the desired step
      // typically h = fs x readout period
      h = *x - tbuff[0].front();
      tbuff[0].pop();
      std::cout<<"h: "<<h<<std::endl;
    }

  // compute the backward derivative
  dydx = (*y - ytemp)/h;
  if( std::isnan(dydx) ) dydt[0] = 0.;
  else dydt[0] = dydx;
}

void TimeDerivative::operator()(const std::vector<double>* xv, const std::vector<double>* yv)
{
  BackWardDiff_Harbitrary(xv,yv);
}

void TimeDerivative::operator()(const double* xx, const double* yy)
{
  // const std::vector<double> xv {*xx};
  // const std::vector<double> yv {*yy};
  BackWardDiff_Harbitrary(xx,yy);
}


#ifdef _TEST_
#include <fstream>

int main()
{
  double s = 0.1;
  std::ofstream fout("out.dat");
  TimeDerivative D(4,25);
  for( double t=-100*M_PI; t<=100*M_PI; t+=s )
    {
      const std::vector<double> x {t,t,t,t};
      const std::vector<double> y {sin(t),cos(t),t,3*t+1};
      D(&x,&y);
      const std::vector<double>* dy = D.GetDeriv();
      std::cout<<t<<"\t";
      fout<<t<<",";
      for( size_t i=0; i<y.size(); ++i)
	{
	  std::cout<<y[i]<<","<<dy->at(i)<<"\t";
	  fout<<y[i]<<","<<dy->at(i)<<",";
	}
      std::cout<<"0\n";
      fout<<"0\n";
    }
  fout.close();
  
  return 0;
}
// g++ -o TimeDerivative.exe -Wall -Wextra -O3 -D_TEST_ TimeDerivative.cc

#endif
