/********************************************************************\

  Name:         TimeDerivative.h
  Created by:   A. Capra - TRIUMF

  Implementantion of a Time Derivative
  - using backward difference at O(t)
  - using backward difference at O(t^3)
  - using backward difference at O(t) with arbitrary step

\********************************************************************/


#ifndef _TIMEDERIV_H
#define _TIMEDERIV_H

#include <iostream>
#include <vector>
#include <queue>
#include <cmath>

class TimeDerivative
{
  // f(t): R -> R^n
  // f'(t): R -> R^n
private:
  // dimension of the codomain
  size_t fn;
  // step size (number of samples)
  // also maximum size of the FIFO
  unsigned int fs;
  // variables buffer for derivative
  // buffer is implemented as a FIFO
  // of size fs
  std::vector<std::queue<double>> ybuff;
  std::vector<std::queue<double>> tbuff;
  // derivative
  std::vector<double> dydt;

private:
  // variables buffer for derivative
  std::vector<double> y1;
  std::vector<double> y2;
  std::vector<double> y3;
  std::vector<double> t1;
  std::vector<double> t2;
  std::vector<double> t3;

public:
  TimeDerivative(size_t nmax);
  TimeDerivative(size_t nmax, unsigned int nsteps);
  ~TimeDerivative();

  // Compute 1st derivative using the backward finite difference method with an accuracy of O(t^3) 
  void BackWardDiffO3(const std::vector<double>* xv, const std::vector<double>* yv);
  // Compute 1st derivative using the backward finite difference method
  void BackWardDiff(const std::vector<double>* xv, const std::vector<double>* yv);
  // Compute 1st derivative using the backward finite difference method
  // the step size is arbitrary
  void BackWardDiff_Harbitrary(const std::vector<double>* xv, const std::vector<double>* yv);
  void BackWardDiff_Harbitrary(const double* x, const double* y);

  void operator()(const std::vector<double>* xv, const std::vector<double>* yv);
  void operator()(const double* xx, const double* yy);
 
  inline const std::vector<double>* GetDeriv() const { return &dydt; }
  inline double GetDeriv1() const { if(dydt.size()==1) return dydt[0]; else return 0.;}
};


#endif
