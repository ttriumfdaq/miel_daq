//  scpico_IV.cxx
//
//
//  Last update Giacomo Gallina on 09/04/20.
//
//  History
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
//#include "mfe.h"
//#include "mscb.h"
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

//ROOT LIBRARIES
#include "TROOT.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"




//FOR RS232

#include "rs232.h"


/*-- Main CODE -------------------------------------------------*/

HNDLE hDB, hDD, hControl,hkey; //General MIDAS ODB handle
KEY key; //Key query



//Keithley instrument variables
const std::string devId = "KEITHLEY INSTRUMENTS";
int cport_nr=16;        /* /dev/ttyUSB0 on Linux machine*/
const int min_cport_nr=16;        /* /dev/ttyUSB0 on Linux machine*/
const int max_cport_nr=21;        /* /dev/ttyUSB5 on Linux machine*/
int bdrate=9600;

char mode[]={'8','N','1',0};
char str[2][512];
const int bufsize = 4095;
unsigned char buf[4096];

//Some strange erros that sometimes happens
unsigned char asterish='*';

unsigned char small_dot='.';

char zero_string[]="-0.000.";
int old_value=0;

int busy;


//For average
//Array that contains data to do the mean
double *data_mean;
//Results of current measures
float results[2];
//Gloabal voltage value
float volt_value;
//Global range reached
int global_range;
//Number of time an overflow is detected
int number_overflow;


double start_voltage,stop_voltage,step_voltage;

int polarity,plc_keythley,NUMBER_POINTS_MEAN,NUMBER_DELAY;

//Set new voltage for IV_curve
void set_v_IV(float voltage_to_set) {
 
   
    std::cout<<" "<<std::endl;
    std::cout<<"Changing voltage .."<<std::endl;
    std::cout<<" "<<std::endl;
    
    if (abs(voltage_to_set)<10)//10
        {
            sprintf(str[1], "SOUR:VOLT:RANG 10\n");

        }else if (abs(voltage_to_set)<50)//50
        {
            sprintf(str[1], "SOUR:VOLT:RANG 50\n");

        }else if (abs(voltage_to_set)<500)
        {
            sprintf(str[1], "SOUR:VOLT:RANG 500\n");

        }else
        {
            printf("Error, invalide voltage. Must be -500< V < 500 .  \n");

        }


    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);


    //Set actual voltage
    sprintf(str[1], "SOUR:VOLT %g\n",voltage_to_set);


    std::cout<<"Setting Voltage source "<<str[1]<<std::endl;
    // strcpy(str[0], "SOUR:VOLT 53\n");
    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);
    
    std::cout<<" "<<std::endl;
    
    
    
}
    
//TURN ON/OFF VOLTAGE SOURCE
void v_on_off(int status){

  
      //turn off soruce
      if(status==0){
        
          sprintf(str[1], "SOUR:VOLT:STAT OFF\n");
          
          
      }else if(status==1){
       
          sprintf(str[1], "SOUR:VOLT:STAT ON\n");
                
      }else{
          
          std::cout<<" Error. Values Allowed [0/1]"<<std::endl;
          exit(-1);
          
      }
      
      std::cout<<"Setting Voltage source to "<<str[1]<<std::endl;

      RS232_cputs(cport_nr, str[1]);
      ss_sleep(1000);

    
};

//CHANGE RANGE
void change_range(float voltage){
        
     
    if(voltage<0){
        
        voltage=-voltage;
        
    }
    
        global_range=global_range+1;
    
        if(global_range==1){
            
                
                printf("\nChanging range 2e-9.....\n\n");
            
                strcpy(str[0], "CURR:RANG 2e -9\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(1000);
                

            
        }else if(global_range==2){
            

                
                printf("\nChanging range 20e -9.....\n\n");

                strcpy(str[0], "CURR:RANG 20e -9\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(1000);
            
            
        }else if(global_range==3){
            

                
                printf("\nChanging range 200e -9.....\n\n");

                strcpy(str[0], "CURR:RANG 200e -9\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(1000);
                

            
            
        }else if(global_range==4){
            
            
                
                printf("\nChanging range 2e -6.....\n\n");
            
            
                strcpy(str[0], "CURR:RANG 2e -6\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(1000);


            
        }else if (global_range==5){
            

                
                printf("\nChanging range 20e -6.....\n\n");

                strcpy(str[0], "CURR:RANG 20e -6\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(1000);
                
            
        }else if(global_range==6){
            

                
                printf("\nChanging range 200e -6.....\n\n");

              strcpy(str[0], "CURR:RANG 200e -6\n");
              RS232_cputs(cport_nr, str[0]);
              ss_sleep(1000);
            
            
        }else if(global_range==7){
            

                
                printf("\nChanging range 2e -3.....\n\n");

               strcpy(str[0], "CURR:RANG 2e -3\n");
               RS232_cputs(cport_nr, str[0]);
               ss_sleep(1000);
                //ss_sleep(2700);
                
  
        }else if(global_range==8 && voltage<8){
            
            
         printf("\nChanging range 20e -3.....\n\n");
         strcpy(str[0], "CURR:RANG 20e -3\n");
         RS232_cputs(cport_nr, str[0]);
         ss_sleep(1000);

            
        }
        
        
    }

//FIND RIGTH RANGE
int find_right_range(){
    
    int i,n=0;
    float temp;
    
    
    printf("Find rigth range.....\n\n");
    strcpy(str[0], ":CURR:RANG?\n");
    RS232_cputs(cport_nr, str[0]);
    ss_sleep(1000);


   
    while(1)
        {

            //Wait before to receive the answer
            ss_sleep(1000);
            //RICEVO RISPOSTA
            n = RS232_PollComport(cport_nr, buf, bufsize);


            if(n > 0)
                {
                    buf[n] = 0;

                    for(i=0; i < n; i++)
                        {
                            if(buf[i] < 32)
                                {
                                    buf[i] = '.';
                                }
                        }


                    //Avoid errors
                    if(1){

                        //NO error happens

                        //temp=atof((char*)buf);

                        //temp=(char*)buf;

                        printf("Received %i bytes, Read: %s\n", n, (char *)buf);
                        //printf("pluto=%f\n",temp);



                        if((atof((char*)buf))!=0){
                            break;
                        }


                    }else{
                        //An error happens
                        printf("An error happens....Ship line...\n");
                        break;
                    }
                }

            ss_sleep(1000);

        }

    
    printf("String read=%s\n",buf);
    
    if(strncmp ((char*)buf,"2.100000E-09",12) == 0){
        
        printf("2nA range\n");
        
        return 1;
        
        
    }else if(strncmp ((char*)buf,"2.100000E-08",12) == 0){
        
        printf("20nA range\n");
        
        return 2;
        
        
    }else if(strncmp ((char*)buf,"2.100000E-07",12) == 0){
        
        printf("200nA range\n");
        
        return 3;
        
        
        
        
    }else if(strncmp ((char*)buf,"2.100000E-06",12) == 0){
        
        printf("2uA range\n");
        
        return 4;
        
        
        
    }else if(strncmp ((char*)buf,"2.100000E-05",12) == 0){
        
        printf("20uA range\n");
        
        return 5;
        
        
    }else if (strncmp ((char*)buf,"2.100000E-04",12) == 0){
        
        
        printf("200uA range\n");
        
        return 6;
        
        
    }else if(strncmp ((char*)buf,"2.100000E-03",12) == 0){
        
        printf("2mA range\n");
        
        return 7;
        
    }else if(strncmp ((char*)buf,"2.100000E-02",12) == 0){
        
        printf("20mA range\n");
        
        return 8;

    }else{
        
        printf("Problems with range....\n");
        
        return 0;
        
    }
    
    
    
    
    
    
    
}

//DO AVERAGE
float read_curr_mean_manual_range(float voltage, int pol){
        
        
        int i,u,k,n,s;
        int range_change;
        double average,average2, variance, std_deviation, sum, sum1, sum2;
        int reference_range,curr_range;
        
        int problem;
        
        double value_2;
        
        i=0;
        sum = 0.;
        sum1 = 0.;
        sum2= 0.;
        number_overflow=0;

        
        if(voltage<0){
            
            voltage=-voltage;
            
        }
        
        
        //std::cout<<" NUMBER_POINTS_MEAN "<<NUMBER_POINTS_MEAN<<std::endl;
        
        for(s=0;s<NUMBER_POINTS_MEAN;s++){
            
            printf("\n");
            printf("s=%d\n",s);
            problem=0;
            
        
            printf("Global_range=%d\n",global_range);
            
       
            
            strcpy(str[0], "READ?\n");
            RS232_cputs(cport_nr, str[0]);
            ss_sleep(900);
            

         while(1)
                {

                    //Wait before to receive the answer
                    ss_sleep(1000);
                    //RICEVO RISPOSTA
                    n = RS232_PollComport(cport_nr, buf, bufsize);


                    if(n > 0)
                        {
                            buf[n] = 0;

                            for(i=0; i < n; i++)
                                {
                                    if(buf[i] < 32)
                                        {
                                            buf[i] = '.';
                                        }
                                }


                            //Avoid errors
                            if(1){

                                //NO error happens

                                //temp=atof((char*)buf);

                                //temp=(char*)buf;

                                printf("Received %i bytes, Read: %s\n", n, (char *)buf);
                                //printf("pluto=%f\n",temp);



                                if((atof((char*)buf))!=0){
                                    break;
                                }


                            }else{
                                //An error happens
                                printf("An error happens....Ship line...\n");
                                break;
                            }
                        }

                    ss_sleep(1000);

                }


            //Find the A of AMPERE in the Answer, obtain a pointer to that memory adress and put in it a zero
            char *c = strchr((char*)buf, 'A');
            if (c != NULL){
                *c = 0;
            }
            else{

                return 0;
            }
            
            value_2 = atof((char*)buf);
            
    
            //Find if the measure produces an error and/or an overflow
            
            while(value_2==9.9e+37 ||value_2==9.91e+37 ){
                
                
                if(value_2==9.9e+37) printf("Overflow..\n");
                if(value_2==9.91e+37) printf("Error..\n");
                
                problem=1;
                
                number_overflow=number_overflow+1;
                printf("Number_overflow=%d\n",number_overflow);
                printf("Global_range=%d\n",global_range);
                
                //Esco dal while
                if(number_overflow==7){
                    
                    break;
                    
                }
                
                //Change the global range
                change_range(voltage);
                
                //Lettura del nuovo valore di corrente
                strcpy(str[0], "READ?\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(900);
               

            while(1)
                   {

                       //Wait before to receive the answer
                       ss_sleep(1000);
                       //RICEVO RISPOSTA
                       n = RS232_PollComport(cport_nr, buf, bufsize);


                       if(n > 0)
                           {
                               buf[n] = 0;

                               for(i=0; i < n; i++)
                                   {
                                       if(buf[i] < 32)
                                           {
                                               buf[i] = '.';
                                           }
                                   }


                               //Avoid errors
                               if(1){

                                   //NO error happens

                                   //temp=atof((char*)buf);

                                   //temp=(char*)buf;

                                   printf("Received %i bytes, Read: %s\n", n, (char *)buf);
                                   //printf("pluto=%f\n",temp);



                                   if((atof((char*)buf))!=0){
                                       break;
                                   }


                               }else{
                                   //An error happens
                                   printf("An error happens....Ship line...\n");
                                   break;
                               }
                           }

                       ss_sleep(1000);

                   }


               //Find the A of AMPERE in the Answer, obtain a pointer to that memory adress and put in it a zero
               char *c = strchr((char*)buf, 'A');
               if (c != NULL){
                   *c = 0;
               }
               else{

                   return 0;
               }
               
               value_2 = atof((char*)buf);
                
                
                
                
            }
            
            
            //Esco for e blocco tutto nel caso raggiungo saturazione finale.
            if(number_overflow==7){
                
                break;
                
            }
            
            //Ha rivelato che c'è stato un problema ma non di saturazione finale
            if(problem==1){
                
                printf("Starting again...\n");
                //Resetto il valore della variabile del ciclo for per farlo ripartire dall'inizio
                i=-1;
                        
               continue;
               
               }
            
            
            
             //Qui si può aggiungere una ulteriore funzione che trova se la misura sta nel range sbagliato e nel caso cambia il range globale
            
            
             printf("All ok for this measure!\n");
            //Convert the string a double
            value_2 = atof((char*)buf);
            
            data_mean[s]=value_2;
            
            printf("Data_mean_1: %lg\n",data_mean[s]);
            
            
        }
        

        
        /*  Compute the sum of all elements */
        
        
        //Mean
        for (u = 0; u < NUMBER_POINTS_MEAN; u++)
        {
            sum2 = sum2 + data_mean[u];
        }
        average = sum2 / (double)(NUMBER_POINTS_MEAN);
        
        
        /* Compute  variance  and standard deviation for freq. */
        for (k = 0; k < u; k++)
        {
            sum1 = sum1 + pow((data_mean[k] - average), 2.0);
        }
        variance = sum1 / (double)(NUMBER_POINTS_MEAN);
        std_deviation = sqrt(variance);
        
        results[0]=average;
        results[1]=std_deviation/sqrt((double)(NUMBER_POINTS_MEAN));
        
        //Esco for e blocco tutto. Maggiore di 8 perchè a 8 cambio la scala
        if(number_overflow==7 && voltage>8){
            
            
            if(pol==1){
                //Scrivo due numeri per capire cosa è successo
                results[0]=2.5e-3;
                results[1]=0;
                
                
            }else{
            //negative polarity
                //Scrivo due numeri per capire cosa è successo
                results[0]=-2.5e-3;
                results[1]=0;
                
                
            }

        }
        
        //minore di 8 perchè significa che sto facendo il forward bias
        if(number_overflow==7 && voltage<8){
            
            if(pol==1){
                
                //Scrivo due numeri per capire cosa è successo
                results[0]=2.5e-2;
                results[1]=0;
                
            }else{
                
                //Scrivo due numeri per capire cosa è successo
                results[0]=-2.5e-2;
                results[1]=0;
                
            }
            
        }
        
        
        
        printf("Average: %lg\n",results[0]);
        printf("Std deviation: %lg\n",results[1]);
        
        return 1;
        
        
    }
    



int main()
    {

        
        
        printf("\n\n\
                   ****************************************************\n\
                   *                                                  *\n\
                   *           MIDAS IV KEYTHLEY - PICOAMMETER        *\n\
                   *                                                  *\n\
                   ****************************************************\n\n");

        int status = 1;
        int size;
        char set_str[80];
        
        
        //CONNECT TO THE ODB
        status=cm_connect_experiment("","","mhttpd",NULL);
            
        if(status!= SUCCESS) exit(1);
        
        status=cm_get_experiment_database(&hDB, NULL);
        

        std::cout<<"Import from file: INPUT_FILE_IV_KEYTHLEY.txt ...."<<std::endl;
        
        // --- Open Input file
        std::ifstream parFile("./INPUT_FILE_IV_KEYTHLEY.txt");
        
        if (parFile.is_open())
        {

            std::cout<<"Import Parameters ..."<<std::endl;
            
            parFile>>start_voltage>>stop_voltage>>step_voltage>>plc_keythley>>NUMBER_POINTS_MEAN>>NUMBER_DELAY;
            
        }else{
            
            std::cout<<"Import file not found !"<<std::endl;
            exit(0);
            
        }
        
        parFile.close();
        
        

        //Connect to Instrument
        
        bool connected = false;
        for(cport_nr = min_cport_nr; cport_nr <= max_cport_nr; cport_nr++){
            printf("Connection Port=%d\n",cport_nr);
            connected = (RS232_OpenComport(cport_nr, bdrate, mode) == 0);
            if(connected){
                strcpy(str[0], "*IDN?\n");
                RS232_cputs(cport_nr, str[0]);
                ss_sleep(1000);
                int n = RS232_PollComport(cport_nr, buf, bufsize);
                std::cout << "Received " << n << "characters, : " << buf << std::endl;
                std::string str = (char*)buf;
                if(str.find(devId) == std::string::npos){
                    RS232_CloseComport(cport_nr);
                    connected = false;
                } else {
                    std::cout << "Connection with instrument succeed" << std::endl;
                    break;
                }
            }
        }
        

        if(!connected){
            printf("Cannot open SCPICO, check connection or device!\n");
            return(0);
        }
        
        
                
       
        
        //------------------------------------------- Create ROOT File and check input file
        
        std::string filePath = "/daq/daqstore/phaar/DATA_PHAAR/IV/";
               
        std::string fileName = "IV.root";
        
        std::string total=filePath+fileName;
        
        
        char char_array[100];
        strcpy(char_array, total.c_str());

            
        //Open the TFile
        TFile IV_analysis(char_array,"recreate");
        
        
        std::cout<<"Imported the following parameters ..."<<std::endl;
                  
                  std::cout<<" "<<std::endl;
                  std::cout<<" start_voltage [V]= "<<start_voltage<<std::endl;
                  std::cout<<" stop_voltage [V]= "<<stop_voltage<<std::endl;
                  std::cout<<" step_voltage [V]= "<<step_voltage<<std::endl;
                  std::cout<<" plc_keythley [#]= "<<plc_keythley<<std::endl;
                  std::cout<<" NUMBER_POINTS_MEAN [#]= "<<NUMBER_POINTS_MEAN<<std::endl;
                  std::cout<<" Initial loops for stabilization = "<<(NUMBER_DELAY)<<std::endl;
                  
                  std::cout<<" The ROOT file created is in "<<total<<std::endl;
                  std::cout<<" "<<std::endl;
                  
                  char a[50];
                  char answer[50];
                  char answer2[50];
                  
                  printf("Is it all right?....\n");
                  printf("Type: YES to continue.....\n");
                  scanf("%s", &a);
              
                  strcpy(answer, "YES");
                  strcpy(answer2, "yes");
                  
                   if(strcmp(a,answer)==0||strcmp(a,answer2)==0){
                  
                       std::cout<<" IV starting ..."<<std::endl;
                       
                   }else{
                       
                       printf("Try again.....\n");
                         
                       cm_disconnect_experiment();
                    
                       return 0;
                       
                   }
        
        //Allocating points for the mean
        printf("Allocating memory for %d points\n",NUMBER_POINTS_MEAN);
                    
                    
                    /* Get memory */
        if (!(data_mean = (double *) malloc ((NUMBER_POINTS_MEAN) * sizeof(double)))){
            printf( "Malloc failed for Data mean array!\n");
            exit(-1);
        }
                    

                
                if(stop_voltage>=0 && start_voltage>=0){
                    
                    polarity=1;
                    printf("\n");
                    printf("Positive polarity selected\n");
                    
                    
                }else if(stop_voltage<0 && start_voltage<=0){
                    
                    polarity=-1;
                    printf("\n");
                    printf("Negative polarity selected\n");
                    
                    
                }else{
                    
                printf("Some errors with polarity....\n");
                    
                cm_disconnect_experiment();
                
                return(0);
                    
                }
        
                
        
        //------------------------------------------- Global Tgraph initialization for histograms

        TGraphErrors* g1 = (TGraphErrors*) gROOT->FindObjectAny("g1");
        if(g1)  g1->Delete();
        std::cout<<"Creating new Tgraph Error for I V data averaged .... "<<std::endl;
        g1 = new TGraphErrors();
	g1->SetName("g1");
	
        //------------------------------------------- Global Tgraph initialization
        
        TGraphErrors* g2 = (TGraphErrors*) gROOT->FindObjectAny("g2");
        if(g2)  g2->Delete();
        std::cout<<"Creating new Tgraph Error for I V data not averaged .... "<<std::endl;
        g2 = new TGraphErrors();
	g2->SetName("g2");
	
        //------------------------------------------- Global Tgraph initialization
               
        TGraphErrors* g3 = (TGraphErrors*) gROOT->FindObjectAny("g3");
        if(g3)  g3->Delete();
        std::cout<<"Creating new Tgraph Error for I vs time.... "<<std::endl;
        g3 = new TGraphErrors();
        g3->SetName("g3");
        
        //------------------------------------------- Global Tgraph initialization
               
        TGraphErrors* g4 = (TGraphErrors*) gROOT->FindObjectAny("g4");
        if(g4)  g4->Delete();
        std::cout<<"Creating new Tgraph Error for initial I V data not averaged.... "<<std::endl;
        g4 = new TGraphErrors();
        g4->SetName("g4");
        
        //------------------------------------------- Global Tgraph initialization
               
        TGraphErrors* g5 = (TGraphErrors*) gROOT->FindObjectAny("g5");
        if(g5)  g5->Delete();
        std::cout<<"Creating new Tgraph Error for initial I vs time.... "<<std::endl;
        g5 = new TGraphErrors();
        g5->SetName("g5");
        
        //------------------------------------------- Global Tgraph initialization
                   
        TGraphErrors* g6 = (TGraphErrors*) gROOT->FindObjectAny("g6");
        if(g6)  g6->Delete();
        std::cout<<"Creating new Tgraph Error for initial I vs data averaged.... "<<std::endl;
        g6 = new TGraphErrors();
        g6->SetName("g6");
        
        
            double volt_start=start_voltage;
            double volt_max=stop_voltage;
            double volt_step=step_voltage;
        
            //For next computation it needs to be positive
            if(step_voltage<0) step_voltage=-step_voltage;
                    

                    printf("Start voltage   = %g V\n",volt_start);
                    printf("Voltage to reach= %g V\n",volt_max);
                    printf("Voltage step [converted to pos.]= %g V\n\n",volt_step);

                    printf("Starting the loop...\n\n");
                    std::cout<<" "<<std::endl;
                    
                    printf("Reset the unit.....\n\n");
                    strcpy(str[0], "*RST\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
                
                    
                    printf("Auto correction OFF.....\n\n");
                    strcpy(str[0], "SYST:ZCH OFF\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
                
                    //Set lowest range for 0 correction
                    printf("CURR:RANG 2e -9.....\n\n");
                    strcpy(str[0], "CURR:RANG 2e -9\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
        
        
                    printf("INIT.....\n\n");
                    strcpy(str[0], "INIT\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
        
        
                    printf("SYST:ZCOR OFF.....\n\n");
                    strcpy(str[0], "SYST:ZCOR OFF\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
        
                    printf("SYST:ZCOR:ACQ.....\n\n");
                    strcpy(str[0], "SYST:ZCOR:ACQ\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
        
                    printf("SYST:ZCOR ON.....\n\n");
                    strcpy(str[0], "SYST:ZCOR ON\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
        
        
                    printf("CURR:RANG:AUTO ON.....\n\n");
                    strcpy(str[0], "CURR:RANG:AUTO ON\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
                    
                    
                    sprintf(str[1], "CURR:NPLC %d\n",plc_keythley);
                    std::cout<<"Setting PLC to "<<str[1]<<std::endl;
                    RS232_cputs(cport_nr, str[1]);
                    ss_sleep(1000);
                            
                    printf("SYST:ZCH OFF.....\n\n");
                    strcpy(str[0],"SYST:ZCH OFF\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
                                            
                    printf("Enabling voltage source.....\n\n");
                    v_on_off(1);
        
               
            
                //NEGATIVE POLARITY
                if(polarity==-1){
                    
                    int counter=0;
                    int counter_2=0;
                    int counter_3=0;
                    
                    for(volt_value=volt_start;volt_value>=volt_max;volt_value=volt_value-volt_step){
                        
                        
                    set_v_IV(volt_value);
                        
                        
                    //Search the initial range
                    if(volt_value==volt_start){
                            

                        printf("\nReading first value to choose range..\n");
                                
                        global_range=find_right_range();
                                
                                if(global_range==0){
                                
                                    printf("An error with range happens...exit\n");
                                    
                                    exit(-1);
                                    
                                    
                                }
                            
                        printf("Auto range OFF....\n");
                        strcpy(str[0], "CURR:RANG:AUTO OFF\n");
                        RS232_cputs(cport_nr, str[0]);
                        ss_sleep(1000);
                                
                                
                                
                            }
                        
                    //Initial stabilitazion
                    if(volt_value==volt_start){
                            
                        printf("\nWait for initial stabilization ...\n");
                        
                        
                        //Save the data during the initial stabilization
                        for(int gg=0;gg<NUMBER_DELAY;gg++){
                            
                            //Update every 10 second
                            ss_sleep(10000);
                            
                            std::cout<<" "<<std::endl;
                            std::cout<<"Initial Stabilization, Iteration "<<gg<<" of "<<NUMBER_DELAY<<std::endl;
                            std::cout<<" "<<std::endl;
                            
                             read_curr_mean_manual_range(volt_value,polarity);
                            
                            
                            //ONLY AVERAGE
                            g6->SetPoint(gg,volt_value,results[0]);
                            g6->SetPointError(gg, 0,results[1]);
                            
                            //ALL THE POINTS
                            for (int ss = 0; ss < NUMBER_POINTS_MEAN; ss++){
                                                   
                            g4->SetPoint(counter_3,volt_value,data_mean[ss]);
                            g4->SetPointError(counter_3, 0,0);
                                               
                            g5->SetPoint(counter_3,counter_3,data_mean[ss]);
                            g5->SetPointError(counter_3, 0,0);
                                               
                            counter_3++;
                                                         
                            }
                            
                            
                            
                            
                        }
                        
                     
                            
                        }
                        
                    //Now read the actual value you need
                    read_curr_mean_manual_range(volt_value,polarity);
                                
                    
                    //ONLY AVERAGE
                    g1->SetPoint(counter,volt_value,results[0]);
                    g1->SetPointError(counter, 0,results[1]);
                        
                    counter++;
                     
                    //ALL THE POINTS
                    for (int ss = 0; ss < NUMBER_POINTS_MEAN; ss++){
                            
                    g2->SetPoint(counter_2,volt_value,data_mean[ss]);
                    g2->SetPointError(counter_2, 0,0);
                        
                    g3->SetPoint(counter_2,counter_2,data_mean[ss]);
                    g3->SetPointError(counter_2, 0,0);
                        
                    counter_2++;
                                  
                    }
                        
                        
                        
                        
                    }
                }
                
                //POSITIVE POLARITY
                if(polarity==1){
                    
                int counter=0;
                int counter_2=0;
                int counter_3=0;
                    
                for(volt_value=volt_start;volt_value<=volt_max;volt_value=volt_value+volt_step){
                    
                    
                    set_v_IV(volt_value);
                                        
                    //Search the initial range
                    if(volt_value==volt_start){
                                                

                    printf("\nReading first value to choose range..\n");
                                                    
                    global_range=find_right_range();
                                                    
                    if(global_range==0){
                                                     
                    printf("An error with range happens...exit\n");
                                                        
                    exit(-1);
                                                        
                                                        
                    }
                                                 
                    printf("Auto range OFF....\n");
                    strcpy(str[0], "CURR:RANG:AUTO OFF\n");
                    RS232_cputs(cport_nr, str[0]);
                    ss_sleep(1000);
                                                    
                                                    
                    }
                    
                    //Initial stabilization
                    if(volt_value==volt_start){
                        
                        printf("\nWait for initial stabilization ...\n");
            
                        
                        //Save the data during the initial stabilization
                        for(int gg=0;gg<NUMBER_DELAY;gg++){
                                                 
                        //Update every 10 second
                        ss_sleep(10000);
                                 
                        std::cout<<" "<<std::endl;
                        std::cout<<"Initial Stabilization, Iteration "<<gg<<" of "<<NUMBER_DELAY<<std::endl;
                        std::cout<<" "<<std::endl;
                            
                        read_curr_mean_manual_range(volt_value,polarity);
                                                 
            
                        //ONLY AVERAGE
                        g6->SetPoint(gg,volt_value,results[0]);
                        g6->SetPointError(gg, 0,results[1]);
                        
                        //ALL THE POINTS
                        for (int ss = 0; ss < NUMBER_POINTS_MEAN; ss++){
                                                                        
                        g4->SetPoint(counter_3,volt_value,data_mean[ss]);
                        g4->SetPointError(counter_3, 0,0);
                                                                    
                        g5->SetPoint(counter_3,counter_3,data_mean[ss]);
                        g5->SetPointError(counter_3, 0,0);
                                                                    
                        counter_3++;
                                                                              
                        }
                                                 
                                                 
                                                 
                                                 
                    }
                                             
                        
                        
                        
                        
                        
                    }
                    
                    //Now read the actual value you need
                    read_curr_mean_manual_range(volt_value,polarity);
                        
                    //ONLY AVERAGE
                    g1->SetPoint(counter,volt_value,results[0]);
                    g1->SetPointError(counter, 0,results[1]);
                        
                    counter++;
                    
                    //ALL THE POINTS
                    for (int ss = 0; ss < NUMBER_POINTS_MEAN; ss++){
                                               
                    g2->SetPoint(counter_2,volt_value,data_mean[ss]);
                    g2->SetPointError(counter_2, 0,0);
                        
                    g3->SetPoint(counter_2,counter_2,data_mean[ss]);
                    g3->SetPointError(counter_2, 0,0);
                                      
                    counter_2++;
                                                     
                    }
                    

                    
                }
            }
                
                
                
        //SOURCE OFF
        v_on_off(0);
                
                
        
        //DISCONNECT FROM ODB
        cm_disconnect_experiment();
        
        //Write on file
        
        std::cout<<"Saving data ..."<<std::endl;
        
        TCanvas* test = new TCanvas("IV","",0.,5000.,500.,500.);
        
        test->SetLeftMargin(0.1);
        test->SetRightMargin(0.04);
        test->SetBottomMargin(0.1);
        test->SetTopMargin(0.1);
        test->SetBorderMode(0);
        test->SetFillColor(0);
        test->Draw();
        
        g1->GetXaxis()->SetTitleOffset(1.2);
        g1->GetYaxis()->SetTitleOffset(1.5);
        g1->SetMarkerStyle(20);
        g1->GetXaxis()->SetTitle("Voltage");
        g1->GetYaxis()->SetTitle("Current Averaged");
        g1->Draw("AP");
        //g1->SetName("g1");
        g1->Write();
        
        
        g2->GetXaxis()->SetTitleOffset(1.2);
        g2->GetYaxis()->SetTitleOffset(1.5);
        g2->SetMarkerStyle(20);
        g2->GetXaxis()->SetTitle("Voltage ");
        g2->GetYaxis()->SetTitle("All - Current");
        g2->Draw("AP");
        //g2->SetName("g2");
        g2->Write();
        
        
        g3->GetXaxis()->SetTitleOffset(1.2);
        g3->GetYaxis()->SetTitleOffset(1.5);
        g3->SetMarkerStyle(20);
        g3->GetXaxis()->SetTitle("Iteration ");
        g3->GetYaxis()->SetTitle("All - Current");
        g3->Draw("AP");
        //g3->SetName("g3");
        g3->Write();
        
        
        g4->GetXaxis()->SetTitleOffset(1.2);
        g4->GetYaxis()->SetTitleOffset(1.5);
        g4->SetMarkerStyle(20);
        g4->GetXaxis()->SetTitle("Voltage ");
        g4->GetYaxis()->SetTitle("All - Current Initial");
        g4->Draw("AP");
        //g4->SetName("g4");
        g4->Write();
        
        
        g5->GetXaxis()->SetTitleOffset(1.2);
        g5->GetYaxis()->SetTitleOffset(1.5);
        g5->SetMarkerStyle(20);
        g5->GetXaxis()->SetTitle("Iteration ");
        g5->GetYaxis()->SetTitle("All - Current Initial");
        g5->Draw("AP");
        //g5->SetName("g5");
        g5->Write();
        
        
        g6->GetXaxis()->SetTitleOffset(1.2);
        g6->GetYaxis()->SetTitleOffset(1.5);
        g6->SetMarkerStyle(20);
        g6->GetXaxis()->SetTitle("Voltage ");
        g6->GetYaxis()->SetTitle("Averaged Current Initial");
        g6->Draw("AP");
	// g6->SetName("g6");
        g6->Write();
        
        
       
        //WRITE ON FILE
        IV_analysis.Write();

        //Close file
        IV_analysis.Close();
        
    return 0;
        
    }
