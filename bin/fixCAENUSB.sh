#!/bin/bash
if [[ "$(whoami)" == "root" ]]; then
    cd /homelocal/miel-daq/packages/CAENUSBdrvB-1.5.4
    make install
    cd -
else
    echo "Please run me as ROOT!"
fi
