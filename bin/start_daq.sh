#!/bin/bash
# start_daq.sh
cd $HOME/online
#
case `hostname` in daq14*)
   echo "Good, we are on daq14!"
   ;;
*)
   echo "The start_daq script should be executed on daq14"
   exit 1
   ;;
esac
#

odbedit -c "set 'Alarms/Alarm system active' n"
# kill existing instances of Midas programs
progs=(mhttpd mlogger mserver feRTD fePowerSwitch fePressure LabViewDriver)
for p in ${progs[@]}; do
    ps=$(pgrep -a $p)
    if [ -n "$ps" ]; then
	echo "Terminating $p, affects:"
	echo $ps
	killall $p
    fi
done

sleep 0.5

for p in ${progs[@]}; do
    if pgrep $p >/dev/null; then
	echo Killing $p
	killall -9 $p
    fi
done

sleep 0.25

zombie=0
for p in ${progs[@]}; do
    if pgrep $p >/dev/null; then
	zombie=$[zombie+1]
	echo "Couldn't kill $p!"
    fi
done

if [ $zombie -gt 0 ]; then
    echo "$zombie zombie processes, manual intervention required."
    odbedit -c "set 'Alarms/Alarm system active' y"
    exit 2
fi

odbedit -c clean
#   start mhttpd on default port. (Mongoose https version - see mhttpd for other options)
mhttpd  -D  -a localhost -a daq14.triumf.ca # optionally restrict access to specified hosts
#
#   start mserver on default port (use argument -p to use a different port) 
mserver -D   # access must now be specifically allowed - see above

# OR (older MIDAS versions)
# mhttpd  -p 8081 -D -a localhost -a XXX.triumf.ca      # optionally restrict access to specified hosts
# mserver -p 7071 -D -a localhost -a lxdragon01.triumf.ca -a lxdragon02.triumf.ca -a XXX.triumf.ca # optionally restrict access to specified hosts

#
mlogger -D

lazylogger -c miel -D

if [ $(pgrep -c phidget22networkserver) -eq 0 ]; then
    phidget22networkserver -D
fi

odbedit -c "set 'Alarms/Alarm system active' y"
#end file
