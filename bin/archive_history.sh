#!/bin/sh

# tmpfile=$(mktemp)

# # find history files older than 2 days
# find $HOME/online/data/history -type f -mtime +2 > $tmpfile

# # move them to daqstore
# rsync -a --files-from=$tmpfile $HOME/online/data/history/ /daq/daqstore/vera/data2/history/ --log-file=$HOME/online/rsync.log

# rm -f $tmpfile

rsync -a $HOME/online/data/history/ /daq/daqstore/vera/data2/history/ --log-file=$HOME/online/rsync.log
