#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo $DIR
odbedit -c 'msg midas user Alarm "Sending Alarm email"'
mapfile -t addr < <(grep -v '#' $DIR/../notify.txt)
/bin/echo $1 | /usr/bin/mail -s MIDAS_ALARM ${addr[@]} 2> $DIR/error.log && odbedit -c 'msg midas user Alarm "Alarm email was sent"' || odbedit -c 'msg midas error Alarm "Alarm email was NOT sent"' 
