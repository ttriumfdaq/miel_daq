//
// fePowerSwitch
//
// Frontend for interfacing with netBooter PowerSwitch over TCP
//

#include <stdio.h>
#include <signal.h> // SIGPIPE
#include <assert.h> // assert()
#include <stdlib.h> // malloc()
#include <iostream>
#include <iomanip>              // to change stream formatting
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <set>

#include "midas.h"
#include "tmfe_rev0.h"

#include "KOtcp.h"
#include "feTCP.h"

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;

void callback(INT hDB, INT hkey, INT index, void *feptr);

/**
 * \brief helper function to split a string into a vector of strings
 */
vector<string> split(const string& str, const string& delim)
{
   vector<string> tokens;
   size_t prev = 0, pos = 0;
   do
      {
         pos = str.find(delim, prev);
         if (pos == string::npos) pos = str.length();
         string token = str.substr(prev, pos-prev);
         if (!token.empty()) tokens.push_back(token);
         prev = pos + delim.length();
      }
   while (pos < str.length() && prev < str.length());
   return tokens;
}

int add_key(HNDLE hDB, HNDLE hkey, KEY *key, INT level, void *pvector){
   if(key->type != TID_KEY)
      ((vector<KEY>*)pvector)->push_back(*key);
   return DB_SUCCESS;
}

/**
 * \brief Generic class for TCP/IP communication with PowerSwitch server program.
 *
 *
 * Some ODB settings are always present, not dependent on the configuration of the PowerSwitch server:
 * @param hostname IP or hostname of the PowerSwitch server
 * @param port port PowerSwitch is listening on
 */
class fePowerSwitch :
   public feTCP
{
public:
   fePowerSwitch(TMFE* mfe /**< [in]  Midas frontend interfacing class. */,
                 TMFeEquipment* eq /**< [in]  Midas equipment class. */):feTCP(mfe, eq, true) // ctor
   {
      fMfe = mfe;
      fEq  = eq;
      fEventSize = 0;
      fEventBuf  = NULL;
   }

   /** \brief Variable initialization. */
   void Init()
   {
      fEq->fOdbEqVariables->RBA("Outlet", &outlet, true, n_outlets);
      fEq->fOdbEqVariables->RDA("Current", &current, true, 2);
      fEq->fOdbEqSettings->RBA("Outlet", &outlet, true, n_outlets);

      // outlet.clear();         // workaround for MIDAS bug in midasodb.cxx
      // fEq->fOdbEqSettings->RBA("Outlet", &outlet);

      if(!Connect()){
         exit(1);
      }
      
      // Turn all outlets off
      for(unsigned int i = 0; i < n_outlets; i++)
         SwitchOutlet(i, false);
      
      outlet = GetOutletStates();
      fEq->fOdbEqSettings->WBA("Outlet", outlet);
      fEq->fOdbEqVariables->WBA("Outlet", outlet);

      SetStatus();

      char tmpbuf[80];
      sprintf(tmpbuf, "/Equipment/%s/Settings/Outlet", fMfe->fFrontendName.c_str());
      HNDLE hkey;
      db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
      db_watch(fMfe->fDB, hkey, callback, (void*)this);
      
   }

   /** \brief Periodic operations, reading variables from PowerSwitch. */
   void HandlePeriodic()
   {
      vector<bool> states = GetOutletStates();
      if(states.size() != n_outlets)
         states = GetOutletStates();
      fEq->fOdbEqVariables->WBA("Outlet", states);
      fEq->fOdbEqVariables->WDA("Current", current);
      SetStatus();
      static unsigned int errcount = 0;
      if(states != outlet){
         fMfe->Msg(MINFO, "Periodic", "Readback doesn't match settings!");
         if(++errcount > 2)
            fMfe->Msg(MERROR, "Periodic", "Readback didn't match settings %d times in a row!", errcount);
         cout << "Readback: ";
         for(bool s: states)
            cout << int(s) << '\t';
         cout << endl;
         cout << "Settings: ";
         for(bool s: outlet)
            cout << int(s) << '\t';
         cout << endl;
      } else {
         errcount = 0;
      }
   }

   /** \brief Function called on ODB setting change, sending variables to PowerSwitch. */
   void fecallback(HNDLE hDB, HNDLE hkey, INT index);
   INT read_event();

   /** \brief Connect to PowerSwitch and confirm identity. */
   bool Connect()
   {
      connected = TCPConnect();
      if(connected) connected = Handshake();
      return connected;
   }

   unsigned int GetVars();

   bool connected = false;
private:
   /** \brief Switch outlet */
   bool SwitchOutlet(unsigned int i, bool state) // FIXME: implement
   {
      std::ostringstream oss;
      oss << "pset " << i+1 << ' ' << int(state) << "\r\n";
      if(verbose) cout << "Sending: " << oss.str() << endl;
      Exchange(oss.str(), false);
      string resp = GetAll();
      if(resp.find('>') == resp.npos){
         fMfe->Msg(MERROR, "SwitchOutlet", "No reply");
         return false;
      } else if (resp.find("Invalid command") != resp.npos){
         fMfe->Msg(MERROR, "SwitchOutlet", "Device didn't understand command: %s", oss.str().c_str());
         return false;
      } else {
         return true;
      }
   }

   /** \brief Confirm server we're talking to is actually PowerSwitch. */
   bool Handshake(){
      Exchange("ver\r\n", false);
      string resp =GetAll();
      if(verbose) cout << "Response: " << resp << endl;
      string expected(">NP-0801DUG2");
      bool correct = (resp.substr(0,expected.size()) == expected);
      if(!correct) fMfe->Msg(MERROR, "Handshake", "Unexpected response: %s", resp.c_str());
      else if(verbose) fMfe->Msg(MINFO, "Handshake", "Handshake successful");
      return correct;
   }

   /** \brief Read outlet states from device
    *
    * Reads out the state of all 8 outlets, also updates the current drawn on the two groups of 4.
    */
   vector<bool> GetOutletStates()
   {
      vector<bool> states(n_outlets);
      // string resp = Exchange("pshow\r\n");
      Exchange("pshow\r\n", false);
      string resp = GetAll();
      if(resp.size() < 100){
         usleep(500000);
         Exchange("pshow\r\n", false);
         resp = GetAll();
      }
      // cout << "STATUS" << endl;
      // cout << resp << endl;
      std::istringstream iss(resp);
      unsigned int linecount(0);
      while(iss.good()){
         string line;
         getline(iss, line, '\r');
         if(line.size() < 5) continue;
         if(line.size()){
            if(line.back()=='\n')
               line.resize(line.size()-1);
            if(line.front()=='\n')
               line.erase(0,1);
         }
         std::istringstream iss2(line);
         int index(0);
         string statestr, name, dummy;
         iss2 >> index >> dummy>> name >> statestr;
         if(iss2.good()){
            linecount++;
            states[index-1] = (statestr==string("ON"));
         } else if(line.find("AC current") != line.npos){
            float curr, maxcurr;
            sscanf(line.c_str(), "AC current draw %d: %f. Max detected %f Amps.", &index, &curr, &maxcurr);
            current[index-1] = curr;
         }
      }
      if(linecount != n_outlets){
         fMfe->Msg(MINFO, "GetOutletStates", "Got wrong number of states: %d != %d", linecount, n_outlets);
         cerr << resp << endl;
      }
      return states;
   }

   void SetStatus()
   {
      std::ostringstream oss;
      oss << "Outlets: ";
      for(unsigned int i = 0; i < outlet.size(); i++){
         if(i) oss << " / ";
         oss << int(outlet[i]);
      }
      fEq->SetStatus(oss.str().c_str(), "lightgreen");
   }

   int verbose = 1;
   const unsigned int n_outlets = 8;
   vector<bool> outlet = vector<bool>(n_outlets, false);
   vector<double> current = vector<double>(2);
};

/** \brief global wrapper for Midas callback of class function
 *
 */
void callback(INT hDB, INT hkey, INT index, void *feptr)
{
   fePowerSwitch* fe = (fePowerSwitch*)feptr;
   fe->fecallback(hDB, hkey, index);
}

void fePowerSwitch::fecallback(HNDLE hDB, HNDLE hkey, INT index)
{
   outlet.clear();         // workaround for MIDAS bug in midasodb.cxx
   fEq->fOdbEqSettings->RBA("Outlet", &outlet);
   bool ok = SwitchOutlet(index, outlet[index]);
   if(!ok){
      fMfe->Msg(MINFO, "fecallback", "Trying again");
      usleep(500000);
      ok = SwitchOutlet(index, outlet[index]);
   }
   if(ok) SetStatus();
   else {
      fEq->SetStatus("Switching error", "magenta");
   }
}


static void usage()
{
   fprintf(stderr, "Usage: fePowerSwitch <name> ...\n");
   exit(1);
}

int main(int argc, char* argv[])
{
   // setbuf(stdout, NULL);
   // setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   std::string name = "";

   if (argc == 2) {
      name = argv[1];
   } else {
      usage(); // DOES NOT RETURN
   }

   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect(name.c_str(), __FILE__);
   if (err.error) {
      printf("Cannot connect, bye.\n");
      return 1;
   }

   //mfe->SetWatchdogSec(0);

   TMFeCommon *common = new TMFeCommon();
   common->EventID = 1;
   common->LogHistory = 1;
   //common->Buffer = "SYSTEM";

   TMFeEquipment* eq = new TMFeEquipment(mfe, name.c_str(), common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   mfe->RegisterEquipment(eq);

   fePowerSwitch *myfe = new fePowerSwitch(mfe, eq);
   mfe->RegisterRpcHandler(myfe);

   myfe->Init();

   if(myfe->connected){
      mfe->RegisterPeriodicHandler(eq, myfe);
      while (!mfe->fShutdownRequested) {
         mfe->PollMidas(10);
      }
   }
   mfe->Disconnect();

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
