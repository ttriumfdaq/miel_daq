#ifndef V1730BOARD_HXX_INCLUDE
#define V1730BOARD_HXX_INCLUDE
#include "midas.h"

extern "C" {
#include "CAENComm.h"
}

/// Class for wrapping the communication with V1730 board.
/// Does setup and data readout.
/// Currently written for a V1730 connected by CONET2 optical interface.
class v1730Board
{
 public:

  v1730Board();
  

  int Connect(); // Connect to device

  int InitializeForAcq(); // Do initial board setup.

  bool ReadReg(DWORD address, DWORD *val);
  bool WriteReg(DWORD address, DWORD val);

  void v1730_AcqCtl(uint32_t operation); // set the acquisition status for board (start run, stop run)
  
  bool FillEventBank(char * pevent);

 private: 

  CAENComm_ErrorCode _WriteReg(DWORD address, DWORD val);
  CAENComm_ErrorCode _ReadReg(DWORD address, DWORD *val);

  int _device_handle;  //!< physical device handle


};

#endif
