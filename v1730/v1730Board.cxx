#include "v1730Board.h"
#include "v1730.h"
#include "stdio.h"
#include<iostream>
#include <stdlib.h>  

v1730Board::v1730Board(){

  _device_handle = -1;
  
};


//This function allows the connection from USB

int v1730Board::Connect(){
  
  printf("Connection to USB\n");
  bool connected = false;
  CAENComm_ErrorCode  sCAEN = CAENComm_GenericError;
  for(int devnum = 0; devnum < 0xffff; devnum++){
    sCAEN = CAENComm_OpenDevice(CAENComm_USB, devnum, 0, 0, &(_device_handle));
    if (sCAEN == CAENComm_Success) {
      printf("Link success, device %d, Module_Handle[%d]\n", devnum, _device_handle);
      connected = true;
      break;
    }
  }
  if(!connected){
    cm_msg(MERROR, "v1730Board::Connect","Error connecting to V1730! %i \n",sCAEN);
    exit(0);
  }
  return 0;
}











//This Function allows the connection from Optic Link

/*
int v1730Board::Connect(){

  printf("enter connection thread\n");
  std::cout << "Opening device (i,l,b) = ("  << ","  << "," <<  ")" << std::endl;
  CAENComm_ErrorCode  sCAEN = CAENComm_OpenDevice(CAENComm_PCIE_OpticalLink, 0, 0, 0, &(_device_handle));
  std::cout << "exit thread ended\n" << std::endl;;
  if (sCAEN == CAENComm_Success) {
    printf("Link success Module_Handle[%d]\n", _device_handle);
    
  }
  else {
    
    cm_msg(MERROR, "v1730Board::Connect","Error connecting to V1730! %i \n",sCAEN);
    exit(0);
  }

  return 0;
}
*/



int v1730Board::InitializeForAcq(){
  return 0;
}


/**
 * \brief   Read 32-bit register
 *
 * \param   [in]  address  address of the register to read
 * \param   [out] val      value read from register
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1730Board::_ReadReg(DWORD address, DWORD *val)
{
  return CAENComm_Read32(_device_handle, address, val);
}
/**
 * \brief   Write to 32-bit register
 *
 * \param   [in]  address  address of the register to write to
 * \param   [in]  val      value to write to the register
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1730Board::_WriteReg(DWORD address, DWORD val)
{
  return CAENComm_Write32(_device_handle, address, val);
}

bool v1730Board::ReadReg(DWORD address, DWORD *val)
{
  return (_ReadReg(address, val) == CAENComm_Success);
}
bool v1730Board::WriteReg(DWORD address, DWORD val)
{
  return (_WriteReg(address, val) == CAENComm_Success);
}




/*****************************************************************/
void v1730Board::v1730_AcqCtl(uint32_t operation)
{
  DWORD reg;
  
  ReadReg(V1730_ACQUISITION_CONTROL,&reg);  
  switch (operation) {
  case V1730_RUN_START:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg | 0x4));
    break;
  case V1730_RUN_STOP:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg & ~(0x4)));
    break;
  case V1730_REGISTER_RUN_MODE:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg & ~(0x3)));
    break;
  case V1730_SIN_RUN_MODE:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg | 0x01));
    break;
  case V1730_SIN_GATE_RUN_MODE:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg | 0x02));
    break;
  case V1730_MULTI_BOARD_SYNC_MODE:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg | 0x03));
    break;
  case V1730_COUNT_ACCEPTED_TRIGGER:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg | 0x08));
    break;
  case V1730_COUNT_ALL_TRIGGER:
    WriteReg(V1730_ACQUISITION_CONTROL, (reg & ~(0x08)));
    break;
  default:
    break;
  }
}

#define MAX_BLT_READ_SIZE_BYTES 10000

bool v1730Board::FillEventBank(char * pevent)
{

  // Temporary: manually generate software trigger 
  //if(1){
  //  WriteReg(0x8108, 1);
  //  usleep(5000);
  //}


  uint32_t  *pdata;

  CAENComm_ErrorCode sCAEN;

  // create bank header
  bk_create(pevent, "V730", TID_DWORD, (void**)&pdata);
  
  DWORD size_remaining_dwords, *data_pos = pdata, to_read_dwords;
  int dwords_read_total = 0, dwords_read = 0;
  
  // Current event size
  
  sCAEN = _ReadReg(V1730_EVENT_SIZE,&size_remaining_dwords);
  //nEvtSze = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_EVENT_SIZE);
  uint32_t sn = SERIAL_NUMBER(pevent);
  if (size_remaining_dwords == 0) { printf("Error S/N:%d Bank Veto ", sn); }
  
  DWORD reg;
  ReadReg(V1730_EVENT_STORED,&reg);
  // printf ("# events %i.  # words %i\n",reg,size_remaining_dwords);

  while (size_remaining_dwords > 0 && sCAEN == CAENComm_Success)
    {
      
      //calculate amount of data to be read in this iteration
      to_read_dwords = size_remaining_dwords > MAX_BLT_READ_SIZE_BYTES/4 ? MAX_BLT_READ_SIZE_BYTES/4 : size_remaining_dwords;
      sCAEN = CAENComm_BLTRead(_device_handle,0, data_pos, to_read_dwords, &dwords_read);
      
      //force an output to see is acquiring
      if (0) std::cout << sCAEN << " = BLTRead(handle=" << _device_handle
				  << ", addr=" << 0
				  << ", data_pos=" << data_pos
				  << ", to_read=" << to_read_dwords
				  << ", dwords_read returned " << dwords_read << ");" << std::endl;
      
      //increment pointers/counters
      dwords_read_total += dwords_read;
      size_remaining_dwords -= dwords_read;
      data_pos += dwords_read;
    }
  

  //  printf("Total words read %i\n",dwords_read_total);
  bk_close(pevent, pdata + dwords_read_total);

  return true;
}
