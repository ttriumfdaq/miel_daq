//
// feRTD.cxx
//
// Frontend for Phidget 4x RTD
//

#include <stdio.h>
#include <signal.h> // SIGPIPE
#include <assert.h> // assert()
#include <stdlib.h> // malloc()
#include <sstream>
#include <iomanip>
#include <math.h>

#include "TimeDerivative.hh"

#include "midas.h"
#include "tmfe_rev0.h"
#include "RTDSensor.hh"


#define NMAX 6

void callback(INT hDB, INT hkey, INT index, void *feptr);

class Myfe :
   public TMFeRpcHandlerInterface,
   public  TMFePeriodicHandlerInterface
{
public:
   TMFE* fMfe;
   TMFeEquipment* fEq;

   std::vector<RTDSensor*> rtd;
   std::vector<int> hubPorts = std::vector<int>(NMAX, -1);
   std::vector<int> wires = std::vector<int>(NMAX, 2);
   std::vector<std::string> type = std::vector<std::string>(NMAX, "RTD_TYPE_PT100_3850");
   std::vector<double> temperature = std::vector<double>(NMAX, 0.);
   std::vector<double> deriv1temp = std::vector<double>(NMAX, 0.);

   int serNum = 0;

private:
   // Create Time Derivative Object
   // for NMAX sensors
   // the derivative is ~60s time steps
   TimeDerivative D = TimeDerivative(NMAX,60);
   
public:
   Myfe(TMFE* mfe, TMFeEquipment* eq) // ctor
   {
      fMfe = mfe;
      fEq  = eq;
   }

   ~Myfe() // dtor
   {
      for(RTDSensor* r: rtd){
         delete r;
      }
      rtd.clear();
      fMfe->Disconnect();
   }

   void Init()
   {
      fEq->fOdbEqSettings->RIA("HubPorts", &hubPorts, true, NMAX);
      fEq->fOdbEqSettings->RI("SerNo", &serNum, true);
      fEq->fOdbEqSettings->RIA("Wires", &wires, true, NMAX);
      fEq->fOdbEqSettings->RSA("Type", &type, true, NMAX);
      
      fEq->fOdbEqVariables->RDA("Temperature", &temperature, true, NMAX);
      temperature = std::vector<double>(NMAX, 0.);
      fEq->fOdbEqVariables->WDA("Temperature", temperature);

      fEq->fOdbEqVariables->RDA("DerivTemp", &deriv1temp, true, NMAX);
      deriv1temp = std::vector<double>(NMAX, 0.);
      fEq->fOdbEqVariables->WDA("DerivTemp", deriv1temp);

      bool success = CreatePhidgets();
      if(success){
         std::cout << "Created" << std::endl;
         success = InitPhidgets();
         if(success) std::cout << "Initialized" << std::endl;
      }
      if(!success){
         for(RTDSensor *r: rtd){
            if(!r->AllGood()){
               fMfe->Msg(MERROR, "Init", "Phidget connection failed! Err: %s", r->GetErrorCode().c_str());
            }
         }
         exit(13);
      }
      char tmpbuf[80];
      sprintf(tmpbuf, "/Equipment/%s/Settings", fMfe->fFrontendName.c_str());
      HNDLE hkey;
      db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
      db_watch(fMfe->fDB, hkey, callback, (void*)this);
      std::cout << "Init done." << std::endl;
   }

   /** \brief Function called on ODB setting change.*/
   void fecallback(HNDLE hDB, HNDLE hkey, INT index)
   {
      std::vector<int> hubPorts_old = hubPorts;
      fEq->fOdbEqSettings->RIA("HubPorts", &hubPorts);
      fEq->fOdbEqSettings->RI("SerNo", &serNum);
      fEq->fOdbEqSettings->RIA("Wires", &wires);
      fEq->fOdbEqSettings->RSA("Type", &type);
      bool success = true;
      if(hubPorts != hubPorts_old)
         success = CreatePhidgets();
      if(success)
         success = InitPhidgets();
      if(!success){
         for(RTDSensor *r: rtd){
            if(!r->AllGood()){
               fMfe->Msg(MERROR, "Init", "Phidget connection failed! Err: %s", r->GetErrorCode().c_str());
            }
         }
         exit(13);
      }
      std::cout << "fecallback done." << std::endl;
   }

   std::string HandleRpc(const char* cmd, const char* args)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
      return "OK";
   }

   void HandleBeginRun()
   {
      fMfe->Msg(MINFO, "HandleBeginRun", "Begin run!");
      fEq->SetStatus("Running", "#00FF00");
   }

   void HandleEndRun()
   {
      fMfe->Msg(MINFO, "HandleEndRun", "End run!");
      fEq->SetStatus("Stopped", "#00FF00");
   }

   void HandlePeriodic()
   {
      // std::cout << "Periodic" << std::endl;
      auto it = rtd.begin();
      std::vector<double> time = std::vector<double>(NMAX, 0.);
      for(unsigned int i = 0; i < hubPorts.size(); i++){
         if(hubPorts[i] >= 0){
            temperature[i] = (*it)->GetTemp();
            time[i] = fMfe->GetTime();
            if(verbose) std::cout << "Temp[" << i << "] = " << temperature[i] << " at " << time[i] << " s" << std::endl;
            it++;
         }
      }
      fEq->fOdbEqVariables->WDA("Temperature", temperature);

      D( &time, &temperature );
      deriv1temp = *D.GetDeriv();
      if(verbose)
         {
            int i=0;
            for( auto& dt: deriv1temp )
               std::cout<<i++<<"  Deriv: "<<dt<<std::endl;
         }
      fEq->fOdbEqVariables->WDA("DerivTemp", deriv1temp);
               
      fEq->SetStatus(StatString().c_str(), "lightgreen");
   }

   std::string StatString(){
      std::ostringstream oss;
      oss << std::setprecision(1) << std::fixed;
      oss << "Running:";
      for(unsigned int i = 0; i < hubPorts.size(); i++){
         if(hubPorts[i] >= 0){
            static bool first = true;
            oss << (first?"":"\t") << temperature[i] << "°C";
            first = false;
         }
      }
      return oss.str();
   }

private:
   bool CreatePhidgets()
   {
      for(RTDSensor* r: rtd){
         delete r;
      }
      rtd.clear();
      bool success = true;
      for(unsigned int i = 0; i < hubPorts.size(); i++){
         if(hubPorts[i] >= 0){
            std::cout << "Creating Phidget " << i << std::endl;
            rtd.push_back(new RTDSensor(hubPorts[i], serNum, true));
            success &= rtd.back()->AllGood();
            if(success)
               std::cout << "Phidget " << i << " created" << std::endl;
            else
               std::cerr << rtd.back()->GetErrorCode() << std::endl;
         }
      }
      return success;
   }
   bool InitPhidgets()
   {
      bool success = true;
      auto it = rtd.begin();
      for(unsigned int i = 0; i < hubPorts.size(); i++){
         if(hubPorts[i] >= 0){
            (*it)->SetNWires(wires[i]);
            PhidgetTemperatureSensor_RTDType rtdtype(RTD_TYPE_PT1000_3850);
            if(type.back() == std::string("RTD_TYPE_PT1000_3850")){
               rtdtype = RTD_TYPE_PT1000_3850;
            } else if(type.back() == std::string("RTD_TYPE_PT100_3850")){
               rtdtype = RTD_TYPE_PT100_3850;
            } else if(type.back() == std::string("RTD_TYPE_PT1000_3920")){
               rtdtype = RTD_TYPE_PT1000_3920;
            } else if(type.back() == std::string("RTD_TYPE_PT100_3920")){
               rtdtype = RTD_TYPE_PT100_3920;
            }
            (*it)->SetType(rtdtype);
            success &= (*it)->AllGood();
            if(success)
               std::cout << "Phidget " << i << " initialized" << std::endl;
            else
               std::cerr << (*it)->GetErrorCode() << std::endl;
            it++;
         }
      }
      return success;
   }

   bool verbose = true;
};

/** \brief global wrapper for Midas callback of class function
 *
 */
void callback(INT hDB, INT hkey, INT index, void *feptr)
{
   Myfe* fe = (Myfe*)feptr;
   fe->fecallback(hDB, hkey, index);
}

static void usage()
{
   fprintf(stderr, "Usage: feRTD <name> ...\n");
   exit(1);
}

int main(int argc, char* argv[])
{
   // setbuf(stdout, NULL);
   // setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   std::string name = "";

   if (argc == 2) {
      name = argv[1];
   } else {
      usage(); // DOES NOT RETURN
   }

   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect("feRTD", __FILE__);
   if (err.error) {
      printf("Cannot connect, bye.\n");
      return 1;
   }

   //mfe->SetWatchdogSec(0);

   TMFeCommon *common = new TMFeCommon();
   common->EventID = 1;
   common->LogHistory = 1;
   //common->Buffer = "SYSTEM";

   TMFeEquipment* eq = new TMFeEquipment(mfe, "feRTD", common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   mfe->RegisterEquipment(eq);

   Myfe* myfe = new Myfe(mfe, eq);

   mfe->RegisterRpcHandler(myfe);

   //mfe->SetTransitionSequenceStart(910);
   //mfe->SetTransitionSequenceStop(90);
   //mfe->DeregisterTransitionPause();
   //mfe->DeregisterTransitionResume();

   myfe->Init();

   mfe->RegisterPeriodicHandler(eq, myfe);

   eq->SetStatus(myfe->StatString().c_str(), "lightgreen");

   while (!mfe->fShutdownRequested) {
      mfe->PollMidas(10);
   }

   mfe->Disconnect();

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
